function init_portfolio_image_excerpt_hack() {


    var excerpt = $('.tdp-portfolio-image-only.full-height');

    if (!excerpt.length) return;


    excerpt.each(function () {
        const obj = $(this),
            parents = obj.parents(),
            container = obj.parents("[data-ispb-cell]"),
            siblings = container.find("> div").length;


        if (siblings !== 1) {
            obj.find('.alt_img').show();
            return;
        }


        parents.each(function () {

            const cell = $(this);

            console.log(cell.attr("data-ispb-cell"));

            if (cell.data("ispb-cell") != null)
                return false;

            cell.addClass("full-height");

        });

    })

}

function init_full_height_vertical_stretch_hack() {

    let port = $(".tdp-vertical-stretch-portfolio, .tdp-horizontal-stretch-portfolio");

    port.each(function () {

        let obj = $(this);

        obj.parentsUntil('.is-post-excerpt').addClass("full-height");

        obj.parents('.is-post-excerpt').addClass("full-height");


    });


}

function init_press_slider() {


    let slider_container = $(".tdp_multiple_images");

    if (!slider_container.length) return;

    slider_container.each(function () {


        let slider = $(this).find(".owl-carousel");

        slider.owlCarousel({
            items: 6,
            margin: 45,
            center: true,
            loop: true
        });
    });
}



function init_cart_quantity_hack(){

    let val = $("input[type=number].pewc-form-field");

    val.attr("step",10).on('blur',function(){
        let valid = this.checkValidity(),
            obj = $(this);


        if(!valid){
            obj.parent().addClass("invalid-integer");
        } else {
            obj.parent().removeClass("invalid-integer");
        }
    })

}

function init_move_envelope_liners() {
    if($('body').hasClass('woocommerce-page')) {
        $('h6:contains("Envelope Liners")').parent().parent().insertBefore($('h6:contains("Envelope Information")').parent().parent());
    }
}


init_portfolio_image_excerpt_hack();

init_full_height_vertical_stretch_hack();

init_press_slider();

init_cart_quantity_hack();

init_move_envelope_liners();