<?php
namespace TSD_Infinisite;

class Portfolio extends IS_Post {

    public function meta() {
        $atts = ['post' => $this];
        return Acme::get_file('components/portfolio/meta.php', $atts);
    }


    public function pin_link($field = 'image') {


        $img = $this->get($field);
        $title = $this->post_title;

        $base = 'http://pinterest.com/pin/create/button/';
        $url = "?url={$this->permalink}";
        $media = "&media={$img['url']}";
        $description = "&description={$title}";


        return $base . $url. $media . $description;



    }


}