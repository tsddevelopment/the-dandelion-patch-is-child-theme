<?

$cols = ['small'  => $this->module['posts_per_row']['small'],
         'medium' => $this->module['posts_per_row']['medium'],
         'large'  => $this->module['posts_per_row']['large'],];

$col_class = '';

foreach ($cols as $size => $val)
    $col_class .= "{$size}-up-{$val} ";

?>

<div class="tdp-portfolio-image-only cell">
    <div class="grid-x <?= $col_class ?> grid-padding-x grid-padding-y">

        <?php
        foreach ($this->query->posts as $wp_post):
            $args = ['post' => new \TSD_Infinisite\IS_Post($wp_post)];
            print \TSD_Infinisite\Acme::get_file('twig/post_excerpts/Portfolio/image-only.php', $args);
        endforeach;
        ?>
    </div>
</div>