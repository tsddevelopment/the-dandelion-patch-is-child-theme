<div class="cell full-height tdp-vertical-stretch-portfolio" style="height: 500px">
    <div class="grid-x full-height">

        <?php foreach ($this->query->posts as $wp_post): ?>
            <? $post = new \TSD_Infinisite\Portfolio($wp_post) ?>
            <? $img = $post->get("image")['sizes']['large'] ?>
            <div class="cell auto flex-column align-center container full-height" style="background-image:url(<?= $img ?>">
                <h2><?= $post->post_title ?></h2>
                <?= $post->meta() ?>
            </div>
        <? endforeach ?>

    </div>
</div>