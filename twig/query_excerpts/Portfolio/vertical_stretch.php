<div class="cell full-height tdp-vertical-stretch-portfolio">
    <div class="grid-y full-height no-margin">

        <?php foreach ($this->query->posts as $wp_post): ?>
            <? $post = new \TSD_Infinisite\Portfolio($wp_post) ?>
            <? $img = $post->get("image")['sizes']['large'] ?>
            <div class="cell auto flex-column align-center container" style="background-image:url(<?= $img ?>">
                <?= $post->meta() ?>
            </div>
        <? endforeach ?>

    </div>
</div>