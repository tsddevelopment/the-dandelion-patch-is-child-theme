<div class="tdp-portfolio-image-with-overlay-text img-overlay">
    <?php foreach ($this->query->posts as $post): ?>
        <? $fields = new \TSD_Infinisite\ACF_Helper($post); ?>
        <? if ($fields->get("image")): ?>
            <img src="<?= $fields->get("image")['sizes']['large'] ?>"/>
            <div class="overlay">
                <? if ($fields->get('excerpt')):
                    echo $fields->get('excerpt');
                endif; ?>
            </div>
        <? endif ?>
    <? endforeach; ?>
</div>