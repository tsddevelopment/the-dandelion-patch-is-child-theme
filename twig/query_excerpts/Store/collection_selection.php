<div class="cell small-12 tdp-collection-selection">

    <div class="grid-x grid-padding-x medium-up-3 align-center">

        <? foreach ($this->query->posts as $c => $post): ?>
            <?
            $post = new \TSD_Infinisite\IS_Post($post);
            $image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'Large Square');
            $large_img_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large');
            $title = $post->ID == 972 ? $post->post_title : "The {$post->post_title} Collection";
            ?>
            <div class="cell text-center">
                <a href="<?= $post->permalink ?>">
                    <img src="<?= $image_url[0] ?>" alt="">
                </a>
                <div class="spacer xsmall"></div>
                <a href="<?= $post->permalink ?>">
                    <h6 class="serif"><?= $title ?></h6>
                </a>
                <div class="spacer"></div>
            </div>

        <? endforeach ?>
    </div>

    <div class="spacer"></div>


</div>