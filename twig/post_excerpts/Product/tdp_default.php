<? $product = new \TSD_Infinisite\IS_Product($post->ID) ?>

<div class="cell">
    <div class="card">
        <img src="<?= $product->image->sizes['medium'] ?>" alt="" class="full-width">
        <div class="card-section">
            <h3><?= $post->post_title ?></h3>
            <div class="grid-x align-bottom">
                <div class="cell shrink text-center">
                    <a href="<?= $post->permalink ?>" class="button">Configure</a>
                </div>
            </div>
        </div>
    </div>
</div>