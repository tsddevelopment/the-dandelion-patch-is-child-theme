<div class="cell tdp-search-result">
    <div class="grid-container grid-x grid-padding-x align-center-middle">
        <div class="cell small-12 medium-3">
            <? if ($post->get("image")): ?>
                <img src="<?= $post->get("image")['sizes']['medium'] ?>" style="float: right" alt="">
            <? endif ?>
        </div>
        <div class="cell auto">
            <h4><?= $post->post_title ?></h4>

            <? if ($post->post_type === 'blog'): ?>

                <h6>Posted on <?= get_the_date('', $post->ID) ?></h6>

            <? endif ?>

            <?= $post->get("excerpt") ?>

            <p><a href="<?= $post->permalink ?>">Read More</a></p>

        </div>
    </div>
</div>