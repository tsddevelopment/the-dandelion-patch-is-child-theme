<?php

$favorites = ['show'           => $post->get("show"),
              'makeup'         => $post->get("makeup"),
              'xm_station'     => $post->get("xm_station"),
              'exercise'       => $post->get("exercise"),
              'clothing_brand' => $post->get("clothing_brand"),
              'holiday'        => $post->get("holiday"),
              'cocktail'       => $post->get("cocktail")];

$id = uniqid('modal_');
?>

<div class="cell is-large-format-blog-excerpt">
    <div class="card">
        <div class="card-section">
            <?= $post->output('name', 'h5', ['class' => 'text-center']) ?>
            <?= $post->output('education') ?>

        </div>
        <div class="text-center" data-open="<?= $id ?>">
            <p class="">Learn More</p>
        </div>
    </div>
</div>


<div class="reveal large" id="<?= $id ?>" data-reveal>

    <?= $post->output('name', 'h2') ?>

    <div class="grid-x small-up-1 medium-up-2 grid-padding-x grid-padding-y">

        <? foreach ($favorites as $type => $value): ?>
            <? if ($value == '')
                continue ?>
            <div class="cell">
                <h5><?= $value ?></h5>
                <p class="no-margin"><?= $type ?></p>
            </div>
        <? endforeach ?>


    </div>

    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
