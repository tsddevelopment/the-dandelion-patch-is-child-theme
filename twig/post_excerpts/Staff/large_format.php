<?
$favorites = ['television'      => $post->get("show"),
              'lipstick'        => $post->get("makeup"),
              'cocktail'        => $post->get("cocktail"),
              'radio'           => $post->get("xm_station"),
              'clock'           => $post->get("exercise"),
              'hanger'          => $post->get("clothing_brand"),
              'calendar'        => $post->get("holiday"),
              'treat'           => $post->get("treat"),
              'activity'        => $post->get("activity"),
              'favorite_client' => $post->get("favorite_client"),];
$label_lookup = ['television'      => "Favorite Show to<br />Binge Watch",
                 'lipstick'        => "Favorite<br />Makeup Brand",
                 'cocktail'        => "Favorite<br />Cocktail",
                 'radio'           => "Favorite<br />Station on XM",
                 'clock'           => "Favorite Form<br />of Exercise",
                 'hanger'          => "Favorite<br />Clothing Brand",
                 'calendar'        => "Favorite<br />Holiday",
                 'treat'           => "Favorite<br />Treat",
                 'activity'        => "Favorite<br />Activity",
                 'favorite_client' => "Favorite<br />Client",];
?>

<div class="cell is-large-format-blog-excerpt">

    <div class="grid-x grid-padding-x align-bottom">
        <div class="cell small-12 medium-4">
            <img src="<?= $post->get("image")['sizes']['large'] ?>" alt="" class="full-width">
        </div>

        <div class="cell small-12 medium-8">
            <h2>
                <?= strtolower($post->get("name")) ?> | <?= strtolower($post->get("title")) ?>
            </h2>

            <? if ($post->get("education")): ?>
                <h6>
                    <?= nl2br($post->get("education")) ?>
                </h6>
            <? else: ?>
                <p><a href="#about_lindsay">Read my bio!</a></p>
                <p style="max-width: 66%">I have had a lifelong love of pretty paper. In fact, when I was really little,
                                          I used to ask my
                                          parents to take me to stationery stores as a special treat and I think we
                                          managed to find one on
                                          every vacation we ever took... <a href="#about_lindsay"
                                                                            style="font-size: 16px; font-weight: normal">read
                                                                                                                         more</a>
                </p>

            <? endif ?>

            <div class="spacer xsmall"></div>

            <? $align = $post->get("name") === 'Lance and Oliver' ? 'align-middle' : 'align-bottom' ?>

            <div class="grid-x grid-padding-x grid-padding-y small-up-2 medium-up-3 <?= $align ?>">
                <? foreach ($favorites as $icon => $answer): ?>
                    <? if (!$answer)
                        continue ?>

                    <? $svg = new \TSD_Infinisite\SVG($icon) ?>
                    <? $svg->set_width(50) ?>
                    <div class="cell text-center">
                        <?= $svg->html() ?>
                        <div class="spacer xsmall"></div>
                        <h6>
                            <?= strtoupper($label_lookup[$icon]) ?>
                        </h6>
                        <h5 class="no-margin">
                            <?= strtoupper($answer) ?>
                        </h5>

                    </div>

                <? endforeach ?>
            </div>
        </div>
    </div>
    <div class="spacer"></div>
</div>
