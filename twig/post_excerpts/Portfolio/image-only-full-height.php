<?
$post = new \TSD_Infinisite\Portfolio($post);
$url = $post->get("image")['sizes']['large']; ?>

<div class="cell full-height tdp-portfolio tdp-portfolio image-only tdp-vertical-stretch-portfolio" style="background-image: url(<?= $url ?>);">
    <?= $post->meta() ?>
</div>