<? $url = $post->get("image")['sizes']['large']; ?>

<div class="cell tdp-portfolio image-with-overlay">
    <img src="<?= $url ?>" alt="<?= $post->title ?>" class="full-width">
    <div class="content">
        <?= $post->get("excerpt") ?>
    </div>
</div>
