<? $post = new \TSD_Infinisite\Portfolio($post) ?>
<? if ($post->get("image")): ?>
    <? $url = $post->get("image")['sizes']['large']; ?>
    <div class="cell tdp-portfolio image-only">
        <div style="background-image:url(<?= $url ?>)" class="full-height img-bg">
            <img src="<?= $url ?>" class="full_width alt_img" />
            <?= $post->meta() ?>
        </div>
    </div>
<? endif ?>
