<? $post = new \TSD_Infinisite\Portfolio($post) ?>
<div class="cell tdp-portfolio tdp-portfolio-image-only">
        <? if ($post->get("image")): ?>
            <? $src = $post->get("image")['sizes']['Large Square']; ?>
            <img src="<?= $src ?>" class="full_width test">
        <? endif ?>
        <?= $post->meta() ?>
</div>
