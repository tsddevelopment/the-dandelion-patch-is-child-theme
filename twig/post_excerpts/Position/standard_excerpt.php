<? $modal_id = uniqid('tdp_position_') ?>
<div class="cell tdp-blog tdp-small-excerpt">
    <div class="grid-x">
        <div class="cell">
            <h4>
                <a href="<?= $post->permalink ?>" class="text-uppercase">
                    <?= $post->post_title ?>
                </a>
            </h4>
            <?= do_shortcode("[divider]") ?>
            <? if ($post->get_start_date()): ?>
                <h6 class="text-uppercase">DATE POSTED: <?= $post->get_start_date()->format("F j, Y") ?></h6>
            <? endif ?>
            <?= $post->get("excerpt") ?>
            <a href="#" class="arrow-link" data-open="<?= $modal_id ?>">Click here to apply</a>
        </div>
    </div>

</div>

<div class="reveal large" id="<?= $modal_id ?>" data-reveal data-editor-style>
    <h2 class="text-center">Apply Online!</h2>
    <p class="text-center">Please fill out the form below. We will be in contact with you shortly.</p>
    <?= do_shortcode("[gravityform id=3 title=false description=false]") ?>
    <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>