<?
$tags = $post->terms['tags'];
$categories = $post->terms['categories'];
$blog_home_id = 87;
$blog_home = get_permalink($blog_home_id);
$tn = get_the_post_thumbnail($post->ID, 'large', ['class' => 'full_width']);
$excerpt = apply_filters("the_content", wp_trim_words($post->wp_obj->post_content, 65));
$date = get_the_date('', $post->ID);

?>
<div class="tdp-blog full-row-excerpt-picture grid-x grid-padding-x align-bottom">
    <div class="cell small-12 medium-4">
        <?= $tn ?>
    </div>
    <div class="cell small-12 medium-8">
        <h3><?= $post->post_title ?></h3>
        <h6><?= $date ?></h6>

        <?= $excerpt ?>


        <? if ($categories): ?>
            <div class="grid-x">
                <div class="cell shrink space-right">
                    <h6>
                        <small>Categories</small>
                    </h6>
                </div>
                <div class="cell auto">
                    <h6>
                        <small>
                            <? foreach ($categories as $c => $category): ?>
                                <a href="/<?= $category->taxonomy ?>/<?= $category->slug ?>"><?= $category->name ?></a><?= $c + 1 != count($categories) ? ', ' : '' ?>
                            <? endforeach ?>
                        </small>
                    </h6>
                </div>
            </div>
        <? endif; ?>



        <? if ($tags): ?>
            <div class="grid-x">
                <div class="cell shrink space-right">
                    <h6>
                        <small>Tagged With</small>
                    </h6>
                </div>
                <div class="cell auto">
                    <h6>
                        <small>
                            <? foreach ($tags as $c => $tag): ?>
                                <a href="/<?= $tag->taxonomy ?>/<?= $tag->slug ?>"><?= $tag->name ?></a><?= $c + 1 != count($tags) ? ', ' : '' ?>
                            <? endforeach ?>
                        </small>
                    </h6>
                </div>
            </div>
        <? endif; ?>

        <div class="spacer small"></div>

        <a class="button hollow no-margin" href="<?= $post->permalink ?>">READ MORE</a>


    </div>
</div>
