<?
$tn = get_the_post_thumbnail($post->ID, 'Large Square', ['class' => 'full_width']);
$date = get_the_date('', $post->ID);
$tags = $post->terms['tags'];
$categories = $post->terms['categories'];
?>

<div class="cell tdp-blog tdp-small-excerpt">
    <div class="grid-x">
        <div class="cell text-center">
            <?= $tn ?>
            <div class="spacer small"></div>
            <h5>
                <a href="<?= $post->permalink ?>">
                    <?= $post->post_title ?>
                </a>
            </h5>
            <h6 class="serif"><?= $date ?></h6>

            <? if ($categories || $tags): ?>
                <div class="spacer small"></div>
            <? endif ?>

            <? if ($categories): ?>
                <h6>
                    <small>
                        Categories
                        <? foreach ($categories as $c => $category): ?>
                            <a
                            href="/<?= $category->taxonomy ?>/<?= $category->slug ?>"><?= $category->name ?></a><?= $c + 1 != count($categories) ? ', ' : '' ?>
                        <? endforeach ?>
                    </small>
                </h6>
            <? endif; ?>

            <? if ($tags): ?>
                <h6>
                    <small>
                        Tagged With
                        <? foreach ($tags as $c => $tag): ?>
                            <a
                            href="/<?= $tag->taxonomy ?>/<?= $tag->slug ?>"><?= $tag->name ?></a><?= $c + 1 != count($tags) ? ', ' : '' ?>
                        <? endforeach ?>
                    </small>
                </h6>
            <? endif; ?>

        </div>
    </div>

</div>