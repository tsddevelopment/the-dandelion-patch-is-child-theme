<?
$tn = get_the_post_thumbnail($post->ID, 'large', ['class' => 'full_width']);
$tags = $post->terms['tags'];
$categories = $post->terms['categories'];
$blog_home_id = 87;
$blog_home = get_permalink($blog_home_id);
$excerpt = apply_filters("the_content", wp_trim_words($post->wp_obj->post_content, 65));
$date = get_the_date('', $post->ID);

?>
<div class="tdp-blog gray_xlight-background grid-x align-middle">
    <div class="cell small-12 medium-auto space-left space-right">
        <h2 class="gray_xxdark-text no-margin">
            <a href="<?= $blog_home ?>">
                Wedding Wednesday
            </a>
        </h2>
        <?= do_shortcode("[divider short gray_dark]") ?>
        <h4><?= $post->post_title ?></h4>


        <div style="padding: 25px;">

            <h6><?= $date ?></h6>

            <?= $excerpt ?>


            <? if ($categories): ?>
                <div class="grid-x">
                    <div class="cell shrink space-right">
                        <h6>
                            <small>Categories</small>
                        </h6>
                    </div>
                    <div class="cell auto">
                        <h6>
                            <small>
                                <? foreach ($categories as $c => $category): ?>
                                    <a
                                    href="/<?= $category->taxonomy ?>/<?= $category->slug ?>"><?= $category->name ?></a><?= $c + 1 != count($categories) ? ', ' : '' ?>
                                <? endforeach ?>
                            </small>
                        </h6>
                    </div>
                </div>
            <? endif; ?>


            <? if ($tags): ?>
                <div class="grid-x">
                    <div class="cell shrink space-right">
                        <h6>
                            <small>Tagged With</small>
                        </h6>
                    </div>
                    <div class="cell auto">
                        <h6>
                            <small>
                                <? foreach ($tags as $c => $tag): ?>
                                    <a
                                    href="/<?= $tag->taxonomy ?>/<?= $tag->slug ?>"><?= $tag->name ?></a><?= $c + 1 != count($tags) ? ', ' : '' ?>
                                <? endforeach ?>
                            </small>
                        </h6>
                    </div>
                </div>
            <? endif; ?>
            <div class="spacer small"></div>
            <a class="arrow-link" href="<?= $post->permalink ?>">Read More</a>
        </div>


    </div>

    <div class="cell small-12 medium-8">
        <?= $tn ?>
    </div>
</div>
