<?php
TSD_Infinisite\Acme::header();

$layout = new \TSD_Infinisite\Layout(get_the_ID());
$post = new \TSD_Infinisite\IS_Post(get_the_ID());

$is_std = $post->ID === 972;


$hero_title  = !$is_std ? "The " . $post->post_title . ' Collection' : $post->post_title;

$module_config = ['acf_fc_layout' => 'hero',
                  'title'         => $hero_title];


$hero_module = new \TSD_Infinisite\ACF_Module(['module'          => $module_config,
                                               'module_template' => $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/dande_child/module_templates/hero/interior/tdp_default.php',]);

$photo_gallery = [];

foreach($post->get("gallery") as $c=>$img):
    $row = $c >= 3 ? 0 : 1;
    $photo_gallery[$row][] = $img;
endforeach;

// checking for save the dates


if($is_std)
$photo_gallery = [$post->get("gallery")];

$gallery_rows = $is_std ? 4 : 6;

?>

<?= $layout->header->get_outside(); ?>


    <div data-editor-style>
        <?= $hero_module->get_html() ?>
    </div>

    <div class="spacer"></div>


<? if (count($photo_gallery)): ?>

    <div class="is-page-builder-wrapper grid-y" data-editor-style>

        <div class="tdp-product-single-gallery gray_xxlight-background">
            <div class="spacer small"></div>
            <div class="grid-container">
                <? foreach ($photo_gallery as $row): ?>
                    <div class="grid-x grid-padding-x grid-padding-y small-up-2 medium-up-3 large-up-<?= $gallery_rows ?> align-center no-margin">

                        <? foreach ($row as $image): ?>
                            <div class="cell text-center">
                                <a href="<?= $image['sizes']['large'] ?>" data-fancybox="gallery"
                                   data-caption="<?= $image['title'] ?> <span><?= $image['caption'] ?></span>"
                                >
                                    <img src="<?= $image['sizes']['medium'] ?>" alt="">
                                </a>
                                <div class="spacer small"></div>
                                <? if ($image['title']): ?>
                                    <h5 class="no-margin"><?= $image['title'] ?></h5>
                                <? endif ?>
                                <? if ($image['caption']): ?>
                                    <p class="no-margin"><?= $image['caption'] ?></p>
                                <? endif ?>
                            </div>


                        <? endforeach ?>
                    </div>
                <? endforeach ?>
                <div class="spacer small"></div>
            </div>
        </div>
        <div class="spacer show-for-medium"></div>
    </div>

<? endif ?>


<?php while (have_posts()) : the_post(); ?>

    <?php wc_get_template_part('content', 'single-product'); ?>

<?php endwhile; ?>

<? do_action('woocommerce_after_main_content'); ?>

<? do_action('woocommerce_sidebar'); ?>


<?= $layout->footer->get_outside_footer(); ?>

<? TSD_Infinisite\Acme::footer();
