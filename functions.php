<?

if (!class_exists("\TSD_Infinisite\Acme"))
    return;

function remove_product_editor() {
    remove_post_type_support( 'product', 'editor' );
}
add_action( 'init', 'remove_product_editor' );

function remove_short_description() {
    remove_meta_box( 'postexcerpt', 'product', 'normal');
}
add_action('add_meta_boxes', 'remove_short_description', 999);

include_once('assets/class/Portfolio.php');

function add_scripts() {
    wp_enqueue_script("trackduck", "//cdn.trackduck.com/toolbar/prod/td.js");
}


function add_id_to_script($tag, $handle, $src) {
    if ($handle == 'trackduck')
        $tag = '<script type="text/javascript" src="' . $src . '" async data-trackduck-id="5addf2df483f464176e64727"></script>';

    return $tag;
}

add_image_size('Large Square', 600, 600, true);


add_action("wp_enqueue_scripts", "add_scripts");
add_filter('script_loader_tag', 'add_id_to_script', 10, 3);

//remove_filter( 'the_content', 'wpautop' );
//remove_filter( 'the_excerpt', 'wpautop' );


\TSD_Infinisite\Shortcode::create('tdp_instagram_gallery');
\TSD_Infinisite\Shortcode::create('divider');


\TSD_Infinisite\Editor::add_style(['title'    => 'Large Paragraph',
                                   'classes'  => 'large-paragraph',
                                   'selector' => 'p']);

\TSD_Infinisite\Editor::add_style(['title'    => 'Subheader Paragraph',
                                   'classes'  => 'subheader-paragraph',
                                   'selector' => 'p']);

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);


$GLOBALS['invitation'] = [10  => 3.75,
                          20  => 3.50,
                          30  => 3.30,
                          40  => 3.15,
                          50  => 3.00,
                          60  => 2.85,
                          70  => 2.75,
                          80  => 2.65,
                          90  => 2.40,
                          100 => 2.25,
                          110 => 2.15,
                          120 => 2.00,
                          130 => 2.00,
                          140 => 2.00,
                          150 => 2.00,
                          160 => 2.00,
                          170 => 2.00,
                          180 => 2.00,
                          190 => 2.00,
                          200 => 2.00,
                          210 => 2.00,
                          220 => 2.00,
                          230 => 2.00,
                          240 => 2.00,
                          250 => 2.00,
                          260 => 2.00,
                          270 => 2.00,
                          280 => 2.00,
                          290 => 2.00,
                          300 => 2.00,
                          310 => 2.00,
                          320 => 2.00,
                          330 => 2.00,
                          340 => 2.00,
                          350 => 2.00];
$GLOBALS['return_address'] = [10  => 30.00,
                              20  => 30.00,
                              30  => 30.00,
                              40  => 30.00,
                              50  => 40.00,
                              60  => 40.00,
                              70  => 40.00,
                              80  => 40.00,
                              90  => 40.00,
                              100 => 50.00,
                              110 => 50.00,
                              120 => 65.00,
                              130 => 65.00,
                              140 => 65.00,
                              150 => 65.00,
                              160 => 65.00,
                              170 => 65.00,
                              180 => 65.00,
                              190 => 65.00,
                              200 => 65.00,
                              210 => 65.00,
                              220 => 65.00,
                              230 => 65.00,
                              240 => 65.00,
                              250 => 65.00,
                              260 => 65.00,
                              270 => 65.00,
                              280 => 65.00,
                              290 => 65.00,
                              300 => 65.00,
                              310 => 65.00,
                              320 => 65.00,
                              330 => 65.00,
                              340 => 65.00,
                              350 => 65.00];
$GLOBALS['rsvp_card'] = [10  => 1.70,
                         20  => 1.70,
                         30  => 1.70,
                         40  => 1.70,
                         50  => 1.70,
                         60  => 1.50,
                         70  => 1.50,
                         80  => 1.50,
                         90  => 1.50,
                         100 => 1.50,
                         110 => 1.50,
                         120 => 1.30,
                         130 => 1.30,
                         140 => 1.30,
                         150 => 1.30,
                         160 => 1.30,
                         170 => 1.30,
                         180 => 1.30,
                         190 => 1.30,
                         200 => 1.30,
                         210 => 1.30,
                         220 => 1.30,
                         230 => 1.30,
                         240 => 1.30,
                         250 => 1.30,
                         260 => 1.30,
                         270 => 1.30,
                         280 => 1.30,
                         290 => 1.30,
                         300 => 1.30,
                         310 => 1.30,
                         320 => 1.30,
                         330 => 1.30,
                         340 => 1.30,
                         350 => 1.30];
$GLOBALS['insert_card'] = [10  => 2.50,
                           20  => 2.50,
                           30  => 2.50,
                           40  => 2.25,
                           50  => 2.00,
                           60  => 2.00,
                           70  => 2.00,
                           80  => 2.00,
                           90  => 2.00,
                           100 => 1.60,
                           110 => 1.60,
                           120 => 1.60,
                           130 => 1.60,
                           140 => 1.60,
                           150 => 1.60,
                           160 => 1.60,
                           170 => 1.60,
                           180 => 1.60,
                           190 => 1.60,
                           200 => 1.60,
                           210 => 1.60,
                           220 => 1.60,
                           230 => 1.60,
                           240 => 1.60,
                           250 => 1.60,
                           260 => 1.60,
                           270 => 1.60,
                           280 => 1.60,
                           290 => 1.60,
                           300 => 1.60,
                           310 => 1.60,
                           320 => 1.60,
                           330 => 1.60,
                           340 => 1.60,
                           350 => 1.60];
$GLOBALS['envelope_liner'] = [10  => 1.00,
                              20  => 1.00,
                              30  => 1.00,
                              40  => 1.00,
                              50  => 0.75,
                              60  => 0.75,
                              70  => 0.75,
                              80  => 0.75,
                              90  => 0.75,
                              100 => 0.65,
                              110 => 0.65,
                              120 => 0.50,
                              130 => 0.50,
                              140 => 0.50,
                              150 => 0.50,
                              160 => 0.50,
                              170 => 0.50,
                              180 => 0.50,
                              190 => 0.50,
                              200 => 0.50,
                              210 => 0.50,
                              220 => 0.50,
                              230 => 0.50,
                              240 => 0.50,
                              250 => 0.50,
                              260 => 0.50,
                              270 => 0.50,
                              280 => 0.50,
                              290 => 0.50,
                              300 => 0.50,
                              310 => 0.50,
                              320 => 0.50,
                              330 => 0.50,
                              340 => 0.50,
                              350 => 0.50];
$GLOBALS['belly_band'] = [10  => 1.00,
                          20  => 1.00,
                          30  => 0.90,
                          40  => 0.80,
                          50  => 0.75,
                          60  => 0.75,
                          70  => 0.75,
                          80  => 0.65,
                          90  => 0.65,
                          100 => 0.60,
                          110 => 0.60,
                          120 => 0.50,
                          130 => 0.50,
                          140 => 0.50,
                          150 => 0.50,
                          160 => 0.50,
                          170 => 0.50,
                          180 => 0.50,
                          190 => 0.50,
                          200 => 0.50,
                          210 => 0.50,
                          220 => 0.50,
                          230 => 0.50,
                          240 => 0.50,
                          250 => 0.50,
                          260 => 0.50,
                          270 => 0.50,
                          280 => 0.50,
                          290 => 0.50,
                          300 => 0.50,
                          310 => 0.50,
                          320 => 0.50,
                          330 => 0.50,
                          340 => 0.50,
                          350 => 0.50];
$GLOBALS['shipping'] = [10  => 7.95,
                        20  => 7.95,
                        30  => 7.95,
                        40  => 7.95,
                        50  => 7.95,
                        60  => 7.95,
                        70  => 7.95,
                        80  => 7.95,
                        90  => 7.95,
                        100 => 7.95,
                        110 => 7.95,
                        120 => 7.95,
                        130 => 14.95,
                        140 => 14.95,
                        150 => 14.95,
                        160 => 14.95,
                        170 => 14.95,
                        180 => 14.95,
                        190 => 14.95,
                        200 => 14.95,
                        210 => 14.95,
                        220 => 14.95,
                        230 => 14.95,
                        240 => 14.95,
                        250 => 14.95,
                        260 => 14.95,
                        270 => 14.95,
                        280 => 14.95,
                        290 => 14.95,
                        300 => 14.95,
                        310 => 19.95,
                        320 => 19.95,
                        330 => 19.95,
                        340 => 19.95,
                        350 => 19.95];
$GLOBALS['full_package'] = [10  => 129.50,
                            20  => 231.95,
                            30  => 319.95,
                            40  => 393.95,
                            50  => 457.95,
                            60  => 518.95,
                            70  => 590.45,
                            80  => 651.95,
                            90  => 704.95,
                            100 => 717.95,
                            110 => 772.95,
                            120 => 780.95,
                            130 => 781.95,
                            140 => 835.95,
                            150 => 889.95,
                            160 => 943.95,
                            170 => 997.95,
                            180 => 1051.95,
                            190 => 1105.95,
                            200 => 1159.95,
                            210 => 1213.95,
                            220 => 1267.95,
                            230 => 1321.95,
                            240 => 1375.95,
                            250 => 1429.95,
                            260 => 1483.95,
                            270 => 1537.95,
                            280 => 1591.95,
                            390 => 1645.95,
                            300 => 1699.95,
                            310 => 1758.95,
                            320 => 1812.95,
                            330 => 1866.95,
                            340 => 1920.95,
                            350 => 1974.95];
$GLOBALS['no_band'] = [10  => 119.50,
                       20  => 204.00,
                       30  => 285.00,
                       40  => 354.00,
                       50  => 412.50,
                       60  => 466.00,
                       70  => 530.00,
                       80  => 592.00,
                       90  => 638.50,
                       100 => 650.00,
                       110 => 699.00,
                       120 => 713.00,
                       130 => 702.00,
                       140 => 751.00,
                       150 => 800.00,
                       160 => 849.00,
                       170 => 898.00,
                       180 => 947.00,
                       190 => 996.00,
                       200 => 1045.00,
                       210 => 1094.00,
                       220 => 1143.00,
                       230 => 1192.00,
                       240 => 1241.00,
                       250 => 1290.00,
                       260 => 1339.00,
                       270 => 1388.00,
                       280 => 1437.00,
                       290 => 1486.00,
                       300 => 1535.00,
                       310 => 1584.00,
                       320 => 1633.00,
                       330 => 1682.00,
                       340 => 1731.00,
                       350 => 1780.00];
$GLOBALS['no_band_no_liner'] = [10  => 95.75,
                                20  => 157.50,
                                30  => 219.30,
                                40  => 271.15,
                                50  => 328.00,
                                60  => 372.85,
                                70  => 427.75,
                                80  => 482.65,
                                90  => 537.40,
                                100 => 562.25,
                                110 => 613.15,
                                120 => 655.00,
                                130 => 704.00,
                                140 => 753.00,
                                150 => 802.00,
                                160 => 851.00,
                                170 => 900.00,
                                180 => 949.00,
                                190 => 998.00,
                                200 => 1047.00,
                                210 => 1096.00,
                                220 => 1145.00,
                                230 => 1194.00,
                                240 => 1243.00,
                                250 => 1292.00,
                                260 => 1341.00,
                                270 => 1390.00,
                                280 => 1439.00,
                                290 => 1488.00,
                                300 => 1537.00,
                                310 => 1586.00,
                                320 => 1635.00,
                                330 => 1684.00,
                                340 => 1733.00,
                                350 => 1780.00];






function get_index_value_from_group_array($key, $data = [], $label = 'name') {

    if ($data == [])
        return false;

    foreach ($data as $index_value => $item)
        if ($item[$label] == $key)
            return $index_value;

}


function set_project_image_caption($img) {
    if ($img['description'])
        return $img['description'];

    //    if ($img['title'])
    //        return $img['title'];

    return '';
}

function get_fancybox_links($img, $gallery_name = 'tdp_portfolio_gallery', $class = '') {
    $caption = set_project_image_caption($img);
    $lrg_img_src = $img['sizes']['large'];

    return ["<a 
                href='{$lrg_img_src}' 
                data-fancybox='{$gallery_name}'
                data-caption='{$caption}' 
                class='{$class}'
                data-gallery='test_gallery'>",
            '</a>'];

}


function customtheme_add_woocommerce_support() {
    add_theme_support('woocommerce');
}

add_action('after_setup_theme', 'customtheme_add_woocommerce_support');

if (class_exists('Woocommerce')) {
    add_filter('woocommerce_enqueue_styles', '__return_empty_array');
}

/**
 * Override loop template and show quantities next to add to cart buttons
 */
function tdp_custom_cart_discounts($cart_obj) {
    if (is_admin() && !defined('DOING_AJAX')) {
        return;
    }

    $shipping_qty = 0;
    $global_envelope_count = 0;

    // Iterate through each cart item
    foreach ($cart_obj->get_cart() as $key => $value) {

        $id = $value['product_extras']['product_id'];


        $all_groups = array_values($value['product_extras']['groups']);

        // we can use this as the first one because the quantity field is required,
        // so this will always be available as the first group
        $extra_groups = array_values($value);

        // check to see if the product in question is a save the date collection (id 972)
        if ($id == 972):


            $values = $extra_groups[0]['groups'];

            $options_field_key = '42f107';

            $style_options = $values[$options_field_key];


            $_chk = function($val) use ($style_options) {
                if (!array_key_exists($val, $style_options))
                    return null;

                $type = $style_options[$val]['type'];

                switch ($type) {

                    case 'checkbox':
                        return $style_options[$val]['value'] === '__checked__';
                        break;

                    default:
                        return $style_options[$val]['value'];
                        break;

                }
            };


            //            \TSD_Infinisite\Acme::dd($style_options);
            $quantity = $_chk('cc1d3c');
            $extra_envelope_quantity = $_chk('528bc2');
            $printed_guest_address_amount = $_chk('9cebed');
            $print_guest_address = $_chk('635696');
            $print_return_address = $_chk('78811f');


            $total_envelope_count = $quantity + $extra_envelope_quantity;

            //            \TSD_Infinisite\Acme::dbg($values);

            // setting up the optional fields


            // applying the pricing
            // the base of the pricing is that the cards are $2 each

            //  $2 per card
            $total_price = $quantity * 2;


            // extra envelopes cost $1 each
            if ($extra_envelope_quantity):
                $total_price += $extra_envelope_quantity;
            endif;

            // if the return addresses are printed, they cost .50 per envelope
            if ($print_return_address):
                $return_address_price = .5 * $total_envelope_count;
                $total_price += $return_address_price;
            endif;


            // if we are printing the guest address on the envelopes, there's a 30
            // base setup fee, plus $2/envelope
            if ($print_guest_address):
                $base_setup_fee = 30;
                $guest_address_price = 2 * $printed_guest_address_amount;
                $total_price += $base_setup_fee + $guest_address_price;
            endif;


            $shipping_cost = $GLOBALS['shipping'][$shipping_qty];
            $total_price += $shipping_cost;


            $value['data']->set_price($total_price);


            continue;
        endif;


        $quantity_info = $extra_groups[0]['groups']['14784774f1ec0ac9'];
        $envelope_info = $extra_groups[0]['groups']['7b47e19fab55c1bd'];


        $fields = [];

        foreach ($quantity_info as $info):
            if ($info['label'] == 'Invitation'):
                $shipping_qty = $info['value'];
                $global_envelope_count += $info['value'];
            endif;
            $fields[$info['label']] = $info['value'];
        endforeach;


        $package_price = 0;


        foreach ($fields as $label => $q):
            $lookup_name = strtolower(str_replace(' ', '_', $label));
            $price_per_unit = $GLOBALS[$lookup_name][$q];
            $price = $price_per_unit * $q;
            $package_price += $price;
        endforeach;

        $invitation_quantity = $fields['Invitation'];


        // we need to fish out the envelope field using it's field ID
        // because we're only passing through the variables that have been assigned
        $extra_envelopes = [];
        $envelope_field_id = 'd456790ee758d1bf';
        $extra_envelope_cost_per_unit = 10;

        foreach ($all_groups as $group)
            foreach ($group as $id => $item)
                if ($id == $envelope_field_id)
                    $extra_envelopes = $item;

        $extra_envelope_count = 0;

        if (array_key_exists('value', $extra_envelopes))
            $extra_envelope_count = $extra_envelopes['value'];

        $global_envelope_count += $extra_envelope_count;

        $extra_envelope_cost = $extra_envelope_count * $extra_envelope_cost_per_unit;

        $total_item_cost = $extra_envelope_cost + $package_price;


        $return_address_selected = 0;

        foreach ($envelope_info as $id => $info)
            if ($info['label'] == 'Print Return Addresses?')
                if ($info['value'] == '__checked__')
                    $return_address_selected = 1;

        if ($global_envelope_count > 350)
            $global_envelope_count = 350;

        if ($return_address_selected)
            $total_item_cost += $GLOBALS['return_address'][$global_envelope_count];

        $shipping_cost = $GLOBALS['shipping'][$shipping_qty];
        $total_item_cost += $shipping_cost;

        $value['data']->set_price($total_item_cost);

    }
}

add_action('woocommerce_before_calculate_totals', 'tdp_custom_cart_discounts', 10, 1);


function wc_price_snippet($value) {
    return "<span class='pewc-cart-item-price'> <span class='woocommerce-Price-amount amount'><span class='woocommerce-Price-currencySymbol'>$</span>$value</span></span>";
}

function update_cart_quantity_value($item_data, $cart_item) {


    if ($cart_item['product_extras']['product_id'] == 972):

        $quantity = 0;
        $extra_envelope_count = 0;
        $envelopes_with_guest_address = 0;

        foreach ($item_data as $c => $info):

            switch ($info['name']):

                case 'Save the Date Card Quantity':
                    $quantity = $info['value'];
                    break;

                case 'Extra Envelope Quantity':
                    $extra_envelope_count = $info['value'];
                    break;

                case 'Quantity of Envelopes with Printed Addresses':
                    $envelopes_with_guest_address = $info['value'];
                    break;

            endswitch;


        endforeach;


        $total_envelope_count = $quantity + $extra_envelope_count;
        $envelope_breakdown = "<br />$quantity with the cards, another $extra_envelope_count extra";

        foreach ($item_data as $c => $info):

            switch ($info['name']):

                case 'Save the Date Card Quantity':
                    $price = wc_price_snippet(money_format('%i', $quantity * 2));
                    $item_data[$c]['display'] = "$quantity @ $2.00/ea" . $price;
                    break;

                case 'Design':
                case 'Your Return Address':
                case 'Wording':
                    // these are the fields that don't have any price info, so we return just the value submitted by the user for confirmation
                    $item_data[$c]['display'] = $item_data[$c]['value'];
                    break;


                case 'Guest Addresses on Envelope':
                    $base_price = 30;
                    $print_price = $envelopes_with_guest_address * 2;
                    $price = wc_price_snippet(money_format('%i', $base_price + $print_price));
                    $item_data[$c]['name'] = "Quantity of Envelopes with Printed Guest Addresses";
                    $item_data[$c]['display'] = "$30 Setup +<br />$envelopes_with_guest_address Printed Envelopes @ $2.00/ea" . $price;
                    break;

                case 'Quantity of Envelopes with Printed Addresses':
                    // cost is .50 per envelope, aka half the quantity
                    $cost = $total_envelope_count / 2;
                    $price = wc_price_snippet(money_format('%i', $cost));
                    $item_data[$c]['name'] = "Quantity of Envelopes with Printed Return Addresses";
                    $item_data[$c]['display'] = "$total_envelope_count @ 50¢/ea" . $price;
                    break;

                case 'Print Return Addresses on Envelope':
                    $price = wc_price_snippet(money_format('%i', $total_envelope_count * .5));
                    $item_data[$c]['display'] = "$total_envelope_count @ 50¢/ea $envelope_breakdown " . $price;
                    break;


                case 'Extra Envelope Quantity':
                    $quantity = $info['value'];
                    $price = wc_price_snippet(money_format('%i', $quantity * 1));
                    $item_data[$c]['display'] = "$quantity @ $1.00/ea" . $price;
                    break;


                case "Print Return Addresses":
                    unset($item_data[$c]);
                    break;

            endswitch;


        endforeach;



        $shipping_cost_int = money_format('%.2n', $GLOBALS['shipping'][$quantity]);

        $current_text = $item_data[$c]['display'] . "<p><span class='bold-text'>Shipping:</span><span class='float-right'>\$$shipping_cost_int</span></p>";

        $item_data[$c]['display'] = $current_text;

        return $item_data;

        // end save the date pricing
    endif;

    // begin standard pricing

    $shipping_qty = 0;
    $global_envelope_count = 0;

    // strip pricing data from each item data setting
    foreach ($item_data as $key => $item):

        $lookups = ['Invitation', 'Envelope Liner', 'Belly Band', 'RSVP Card', 'Insert Card'];

        if (!in_array($item['name'], $lookups)):

            if ($item['name'] === 'Extra Envelopes?'):
                $global_envelope_count += $item['value'];
                $cost = money_format("%.2n", $item['value'] * 10);
                $item_data[$key]['display'] = "{$item['value']} @ $10 /ea : <span class='float-right'>\$$cost</span>";
                continue;
            endif;

            if ($item['name'] === 'Print Return Addresses?'):

                $global_envelope_total_for_pricing = $global_envelope_count > 350 ? 350 : $global_envelope_count;
                $cost = money_format('%.2n', $GLOBALS['return_address'][$global_envelope_total_for_pricing]);
                $item_data[$key]['display'] = "$global_envelope_count total envelopes : <span class='float-right'>\$$cost</span>";
                continue;
            endif;

            $item_data[$key]['display'] = $item['value'];
            continue;
        endif;

        // getting the value for the shipping, currently tied to the amount of invitations ordered
        // also incrementing the envelope count for the return address label check
        if ($item['name'] == 'Invitation'):
            $shipping_qty = $item['value'];
            $global_envelope_count += $item['value'];
        endif;

        $lookup_name = strtolower(str_replace(' ', '_', $item['name']));
        $q = $item['value'];
        $price_per_unit = $GLOBALS[$lookup_name][$q];
        $ppu_display = money_format("%.2n", $price_per_unit);
        $price_int = $price_per_unit * $q;

        $price = money_format("%.2n", $price_int);
        $display = "$q @ \$$ppu_display /ea <span class='float-right'>\$$price</span>";

        $item_data[$key]['display'] = $display;
    endforeach;


    $shipping_cost_int = money_format('%.2n', $GLOBALS['shipping'][$shipping_qty]);

    $current_text = $item_data[$key]['display'] . "<p><span class='bold-text'>Shipping:</span><span class='float-right'>\$$shipping_cost_int</span></p>";

    $item_data[$key]['display'] = $current_text;

    return $item_data;
}

add_filter('woocommerce_get_item_data', 'update_cart_quantity_value', 10, 2);

\TSD_Infinisite\Shortcode::create('image_test');

function tsd_acf_init() {
    acf_update_setting( 'google_api_key', 'AIzaSyCLR6hYEegPInq4fykjv56L4NbiKsgcah8' );
}
add_action( 'acf/init', 'tsd_acf_init' );
function udapte_acf_google_maps_api_key( $api ) {
    $api['key'] = 'AIzaSyCLR6hYEegPInq4fykjv56L4NbiKsgcah8';
    return $api;
}
add_filter( 'acf/fields/google_map/api', 'udapte_acf_google_maps_api_key' );