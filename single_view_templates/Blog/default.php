<?

$post = new \TSD_Infinisite\Portfolio(get_the_ID());
$module_config = ['acf_fc_layout' => 'hero',
                  'title'         => 'Wedding Wednesdays'];


$hero_module = new \TSD_Infinisite\ACF_Module(['module'          => $module_config,
                                               'module_template' => $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/dande_child/module_templates/hero/interior/tdp_default.php',]);

$image = $post->get("image");

$tags = $post->terms['tags'];
$cats = $post->terms['categories'];


foreach ($cats as $i => $cat)
    if (in_array($cat->term_id, [10, 12]))
        unset($cats[$i]);


?>

<div data-editor-style>
    <?= $hero_module->get_html() ?>
</div>

<div class="spacer small"></div>

<div class="grid-container" data-editor-style>
    <div class="grid-x grid-padding-x">
        <div class="cell small-12 medium-4">

            <? if ($image): ?>
                <img src="<?= $image['url'] ?>" alt="<?= $post->post_title ?>">
                <div class="spacer small"></div>
            <? endif ?>

            <? if (count($tags)): ?>

                <h5>Tagged:</h5>

                <p>
                    <? foreach ($tags as $tag)
                        print $tag->name . ' ' ?>
                </p>

            <? endif ?>

            <? if (count($cats)): ?>

                <h5>Categories:</h5>

                <p>
                    <? foreach ($cats as $cat)
                        print $cat->name . ' ' ?>
                </p>
            <? endif ?>
        </div>

        <div class="cell small-12 medium-auto">
            <h2 class="primary-text"><?= $post->get("title") ?></h2>
            <h6>Posted: <?= get_the_date("F j, Y", $post->ID) ?></h6>
            <div class="divider"></div>
            <?= apply_filters("the_content", $post->wp_obj->post_content) ?>
            <div class="spacer large"></div>
            <div class="text-center full-width">
                <a href="<?= get_permalink(87) ?>" class="button large">Return to<br />Wedding Wednesdays</a>
            </div>

        </div>
    </div>
</div>

<div class="spacer xlarge"></div>