<?


$term = get_queried_object();

$page = get_query_var('paged');

$query_args = ['post_type'      => 'blog',
               'posts_per_page' => 7,
               'paged'          => $page,
               'tax_query'      => [['taxonomy' => $term->taxonomy,
                                     'field'    => 'slug',
                                     'terms'    => [$term->slug]]]];

$query = new \TSD_Infinisite\IS_Query($query_args);

$posts = $query->posts;

$featured_post = array_shift($posts);

$pagination_args = ['type' => 'list'];

$pagination_html = paginate_links($pagination_args);

$large_template = '/wp-content/themes/dande_child/twig/post_excerpts/Blog/full-row-excerpt-picture.php';
$small_template = '/wp-content/themes/dande_child/twig/post_excerpts/Blog/standard_excerpt.php';

$category_archive = $term->taxonomy === 'categories';

$top_line_text = $category_archive ? 'POSTS CATEGORIZED BY' : 'POSTS TAGGED WITH';


?>


<div class="grid-container is_page_builder_row">

    <div class="grid-x">
        <div class="small-12 cell text-center">
            <h6><?= $top_line_text ?></h6>
            <h2><?= $term->name ?></h2>
            <p></p>
            <div class="divider center"></div>
        </div>
    </div>


    <div class="grid-x grid-padding-x grid-padding-y small-up-1" style="padding: 0 15px;">
        <div class="cell">
            <?= $featured_post->render_php_template($large_template); ?>
            <div class="spacer small"></div>
        </div>
    </div>
</div>

<div class="gray_xlight-background float-left full-width">
    <div class="spacer small"></div>

    <div class="grid-container is_page_builder_row">
        <div class="grid-x grid-padding-x grid-padding-y small-up-1 medium-up-3">
            <? foreach ($posts as $post): ?>

                <div class="cell">
                    <?= $post->render_php_template($small_template); ?>
                </div>


            <? endforeach ?>
            <div class="spacer"></div>
            <div class="text-center full_width">
                <?= $pagination_html ?>
                <div class="spacer small"></div>
                <a href="<?= get_permalink(87) ?>" class="button">Return to Blog</a>
            </div>
        </div>
    </div>
    <div class="spacer small"></div>
</div>