<?
do_action('woocommerce_before_single_product');
if (post_password_required()) {
    echo get_the_password_form(); // WPCS: XSS ok.
    return;
}

// $post    = new \TSD_Infinisite\IS_Post( get_post( get_the_ID() ) );
// $fields  = $post->fields;
$header = $this->header;
$sidebar = $this->sidebar;
$footer = $this->footer;
$product = new \TSD_Infinisite\IS_Product(get_the_ID());
$post = new \TSD_Infinisite\IS_Post(get_the_ID());
$cats = $product->post->terms['categories'];
$product_category_links = '';
$content = $this->get_row_html();

foreach ($cats as $c => $cat):
    $product_category_links .= "<a href='#'>{$cat->name}</a>";
    if ($c + 1 != count($cats))
        $product_category_links .= ", ";
endforeach;

$gallery_template = INFINISITE_URI . 'components/media/photo_gallery/montgomery_lightbox.php';

$module_config = ['acf_fc_layout' => 'hero',
                  'title'         => "The " . $post->post_title . ' Collection'];


$hero_module = new \TSD_Infinisite\ACF_Module(['module'          => $module_config,
                                               'module_template' => $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/dande_child/module_templates/hero/interior/tdp_default.php',]);

$large_img_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large');

?>

<div data-editor-style>
    <?= $hero_module->get_html() ?>
</div>

<div class="spacer"></div>

<div class='is-page-builder-wrapper grid-container grid-y' data-editor-style>
    <div class='cell'>
        <?= $content ?>
    </div>
</div>


<? if ($post->get("gallery")): ?>

    <div class="is-page-builder-wrapper grid-y" data-editor-style>

        <div class="tdp-product-single-gallery gray_xxlight-background">
            <div class="spacer small"></div>
            <div class="grid-container">
                <div class="grid-x grid-padding-x grid-padding-y show-for-medium medium-up-6">

                    <? foreach ($post->get("gallery") as $image): ?>
                        <div class="cell text-center">
                            <a href="<?= $image['sizes']['large'] ?>" data-fancybox="gallery"
                               data-caption="<?= $image['title'] ?> <span><?= $image['caption'] ?></span>"
                            >
                                <img src="<?= $image['sizes']['Large Square'] ?>" alt="">
                            </a>
                            <div class="spacer small"></div>
                            <? if ($image['title']): ?>
                                <h5 class="no-margin"><?= $image['title'] ?></h5>
                            <? endif ?>
                            <? if ($image['caption']): ?>
                                <p class="no-margin"><?= $image['caption'] ?></p>
                            <? endif ?>
                        </div>


                    <? endforeach ?>
                </div>
                <div class="spacer small"></div>
            </div>
        </div>
        <div class="spacer"></div>
    </div>

<? endif ?>


<div class='is-page-builder-wrapper grid-container grid-y' data-editor-style>


    <div class="cell">

        <div class="spacer small"></div>
        <? wc_get_template_part('content', 'single-product') ?>

    </div>

</div>

<div class="spacer xlarge"></div>