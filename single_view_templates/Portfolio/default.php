<?

$post = new \TSD_Infinisite\Portfolio(get_the_ID());
$module_config = ['acf_fc_layout' => 'hero',
                  'title'         => $post->post_title];


$hero_module = new \TSD_Infinisite\ACF_Module(['module'          => $module_config,
                                               'module_template' => $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/dande_child/module_templates/hero/interior/tdp_default.php',]);


$image = $post->get("image");
$categories = $post->terms['product_type'];
$gallery = $post->get("gallery");
?>

<div data-editor-style>
    <?= $hero_module->get_html() ?>
</div>

<div class="spacer"></div>

<div class="grid-container">
    <div class="grid-x grid-padding-x">
        <div class="cell small-12 medium-4 large-shrink">
            <? if ($image): ?>
                <img src="<?= $image['sizes']['large'] ?>" alt="<?= $post->post_title ?>" style="max-width: 100%;">
            <? endif ?>
            <? if (count($categories)): ?>
                <div class="spacer"></div>
                <div class="card">
                    <div class="card-section">
                        <h4>Categories:</h4>
                        <p>
                            <? foreach ($categories as $cat): ?>
                                <?= $cat->name ?>
                            <? endforeach ?>
                        </p>
                    </div>
                </div>
            <? endif ?>
        </div>
        <div class="cell small-12 medium-auto">
            <h4 class="text-center">Ready to Learn More?</h4>
            <?= do_shortcode("[gravityform id='2' title='false' description='true']") ?>
        </div>
    </div>
</div>
<? if ($gallery): ?>
    <div class="gray_xlight-background" style="padding: 45px 0">
        <div class="grid-container">
            <div class="text-center">
                <h3 class="serif secondary-text">take a look</h3>
                <div class="spacer xsmall"></div>
            </div>
            <div class="grid-x small-up-2 medium-up-3 grid-padding-x grid-padding-y">

                <? foreach ($gallery as $image): ?>
                    <div class="cell flex flex-column align-center align-middle">
                        <img src="<?= $image['sizes']['large'] ?>" alt="">
                        <div class="spacer xsmall"></div>
                        <h6><?= $image['title'] ?></h6>
                    </div>

                <? endforeach ?>

            </div>
        </div>
    </div>
<? endif ?>


<div class="cell small-12 text-center">
    <div class="spacer"></div>
    <a href="#" class="button large" onclick="window.history.back()">Back to Previous</a>
    <div class="spacer large"></div>
</div>

