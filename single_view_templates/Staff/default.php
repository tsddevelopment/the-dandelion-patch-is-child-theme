<?

$post = new \TSD_Infinisite\Portfolio(get_the_ID());
$module_config = ['acf_fc_layout' => 'hero',
                  'title'         => 'Wedding Wednesdays'];


$hero_module = new \TSD_Infinisite\ACF_Module(['module'          => $module_config,
                                               'module_template' => $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/dande_child/module_templates/hero/interior/tdp_default.php',]);


$image = $post->get("image");
$categories = $post->terms['product_type'];

$tags = $post->terms['tags'];
$cats = $post->terms['categories'];

$show = $post->get("show");
$makeup = $post->get("makeup");
$xm_station = $post->get("xm_station");
$exercise = $post->get("exercise");
$clothing_brand = $post->get("clothing_brand");
$holiday = $post->get("holiday");
$cocktail = $post->get("cocktail");

$favorites = ['show'           => $show,
              'makeup'         => $makeup,
              'xm_station'     => $xm_station,
              'exercise'       => $exercise,
              'clothing_brand' => $clothing_brand,
              'holiday'        => $holiday,
              'cocktail'       => $cocktail];

?>

<div data-editor-style>
    <?= $hero_module->get_html() ?>
</div>

<div class="spacer"></div>

<div class="grid-container">
    <div class="grid-x grid-padding-x align-center">
        <div class="cell small-12 medium-4">
            <? if ($image): ?>
                <img src="<?= $image['url'] ?>" alt="<?= $post->post_title ?>">
                <div class="spacer small"></div>
            <? endif ?>
            <div class="card text-center">
                <div class="card-section">
                    <h4>Favorites:</h4>
                    <div class="grid-x small-up-1 grid-padding-y medium-up-2">
                        <? foreach ($favorites as $type => $value): ?>
                            <? if ($value == '')
                                continue ?>
                            <div class="cell">
                                <h5><?= $value ?></h5>
                                <p class="no-margin"><?= $type ?></p>
                            </div>
                        <? endforeach ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="cell small-12 medium-6">
            <h2><?= $post->get("name") ?></h2>
            <h5><?= $post->get("title") ?></h5>

            <? if($post->get("education")): ?>
            <h6><?= $post->get("education") ?></h6>

            <? endif ?>

            <?= $post->get("content") ?>
        </div>
    </div>
</div>

<div class="spacer xlarge"></div>