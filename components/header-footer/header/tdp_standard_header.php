<? if ($top_links): ?>
    <div class="top-bar">
        <div class="grid-container">
            <div class="top-bar">
                <div class="top-bar-left">
                    <ul class="dropdown menu" data-dropdown-menu>
                        <li class="menu-text"><?= get_bloginfo("name") ?></li>
                    </ul>
                </div>
                <div class="top-bar-right">
                    <? if (isset($top_links))
                        wp_nav_menu(['menu'       => $top_links,
                                     'menu_class' => 'dropdown menu',
                                     'items_wrap' => '<ul id="%1$s" class="%2$s" data-dropdown-menu>%3$s</ul>',
                                     'walker'     => new TSD_Infinisite\top_menu_bar_walker()]) ?>

                </div>
            </div>
        </div>

    </div>
<? endif ?>


<div class="grid-container fluid no-padding is-standard-desktop-header" is-editor-style>
    <div class="grid-y">
        <div class="cell">
            <div class="spacer xsmall"></div>
        </div>
        <div class="cell">
            <div class="grid-container align-middle">

                <? if ($img): ?>
                    <div class="grid-x align-center-middle show-for-medium">
                        <div class="cell logo shrink padding-bottom">
                            <a href="<?= site_url() ?>">
                                <img src="<?= $img['sizes']['medium'] ?>" alt="Header Logo" />
                            </a>
                        </div>
                    </div>
                <? endif ?>

                <div class="grid-x medium-header align-middle padding-top padding-bottom">


                    <div class="cell show-for-medium auto menus">
                        <div class="grid-x align-center">
                            <? if ($primary_menu): ?>
                            <div class="cell primary-menu-area flex align-middle align-center shrink">
                                <? wp_nav_menu(['menu'       => $primary_menu,
                                                'menu_class' => 'dropdown menu align-right',
                                                'items_wrap' => '<ul id="%1$s" class="%2$s" data-dropdown-menu>%3$s</ul>',
                                                'walker'     => new TSD_Infinisite\top_menu_bar_walker()]) ?>
                            </div>
                            <div class="cell shrink">

                                <div class="grid-x align-right">
                                    <div class="cell shrink">
                                        <? if ($cta_menu): ?>
                                            <?
                                            wp_nav_menu(['menu'       => $cta_menu,
                                                         'menu_class' => 'menu',
                                                         'walker'     => new TSD_Infinisite\top_menu_bar_walker()])
                                            ?>
                                        <? endif ?>
                                    </div>
                                    <div class="cell shrink align-self-middle">

                                        <div class="space-left">
                                            <i class="fas fa-search primary-text" data-open="tdp_header_search"></i>
                                        </div>

                                        <div class="reveal" id="tdp_header_search" data-reveal>
                                            <h3>Search the Patch!</h3>
                                            <?= get_search_form() ?>
                                            <button class="close-button" data-close aria-label="Close modal"
                                                    type="button">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <? endif ?>


                                <? if ($secondary_menu): ?>

                                    <div class="cell small-shrink medium-shrink">

                                        <?
                                        $walker = $secondary_menu_branch_toggle ? new TSD_Infinisite\Display_Parent_Branch() : false;

                                        wp_nav_menu(['menu'       => $secondary_menu,
                                                     'menu_class' => 'dropdown menu',
                                                     'items_wrap' => '<ul id="%1$s" class="%2$s align-right" data-dropdown-menu>%3$s</ul>',
                                                     'walker'     => $walker]) ?>

                                    </div>

                                <? endif ?>

                                <? if ($social_media): ?>
                                    <div class="cell small-shrink medium-shrink social-media">
                                        <?= $social_media ?>
                                    </div>
                                <? endif ?>
                            </div>
                        </div>
                    </div>


                    <? if ($overlay_menu): ?>


                        <div class="cell small-shrink medium-shrink no-padding overlay-menu-launcher">
                            <span class="fa-2x cursor_pointer no_outline"
                                  data-open="overlay-menu-modal"
                                  aria-controls="overlay-menu-modal" aria-haspopup="true" tabindex="0">
                        <?= $overlay_menu_launcher_icon ?>
                            </span>
                        </div>


                        <? include($_SERVER['DOCUMENT_ROOT'] . $overlay_menu_template); ?>


                    <? endif ?>

                    <? if ($img): ?>
                        <div class="cell logo auto padding-bottom hide-for-medium">
                            <a href="<?= site_url() ?>">
                                <img src="<?= $img['sizes']['medium'] ?>" alt="Header Logo" />
                            </a>
                        </div>
                    <? endif ?>


                    <? if ($offcanvas_menu): ?>

                        <div class="cell shrink no-padding offcanvas-menu-launcher hide-for-medium">
                            <span class="fa-2x cursor_pointer no_outline"
                                  data-toggle="offCanvas"
                            >
                                <?= $offcanvas_menu_launcher_icon ?>
                            </span>
                        </div>

                    <? endif ?>

                </div>

            </div>
        </div>

        <? if ($large_menu)
            include($_SERVER['DOCUMENT_ROOT'] . $large_menu_template)
        ?>

        <div class="cell">
            <div class="spacer xsmall"></div>
        </div>
    </div>
</div>