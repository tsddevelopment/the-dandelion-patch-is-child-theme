<? if(!isset($atts)): ?>
<div class="divider"></div>
<? return ?>
<? endif ?>

<? $class = $atts === '' ? '' : join(' ', $atts) ?>
<div class="divider <?= $class ?>"></div>