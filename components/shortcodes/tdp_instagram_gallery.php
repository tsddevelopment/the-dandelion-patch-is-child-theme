<?
$ig = new \TSD_Infinisite\Account('thedandelionpatch');
$ig_link = $ig->get_link();
$posts = $ig->get_posts(15);
?>
<div class="text-center">

    <h3 class="tdp-insta-title">
        <i class="fab fa-instagram grey-text"></i>
        <a class="tertiary-text"
           href="<?= $ig_link ?>"
           target="_blank">@thedandelionpatch</a>
    </h3>
</div>
<div class="spacer"></div>
<div class="footer_gallery white-background" data-editor-style>
    <div class="grid-x grid-padding-x grid-padding-y small-up-3 medium-up-5">
        <? if ($posts): ?>
            <? foreach ($posts as $post): ?>
                <div class="cell">
                    <a
                            data-fancybox="footer_gallery"
                            href="<?= $post['images']['standard_resolution']['url'] ?>"
                            data-caption="<?= $post['caption']['text'] ?>"
                            style="
                                    background-image:  url(<?= $post['images']['standard_resolution']['url'] ?>);
                                    background-position: center;
                                    display: block;
                                    background-size: cover;
                                    "
                    >
                        <img src="<?= $post['images']['standard_resolution']['url'] ?>"
                             class="space-left space-right full-width"
                             style="opacity: 0;"
                        />
                    </a>
                </div>
            <? endforeach ?>
        <? endif ?>
    </div>
</div>