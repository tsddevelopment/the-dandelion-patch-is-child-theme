<?

global $wpdb;

$sql = $wpdb->get_results("
    select * 
    from wp_posts a 
      left join wp_postmeta b 
        on (a.ID = b.post_id) 
      and a.post_type = 'attachment' 
      and b.meta_key = '_wp_attachment_metadata' 
    order by a.ID desc 
        limit 2000
    ");


$image_size = 'medium';
$meta_key = 'credit';


$html = '';
$total_results = count($sql);
$filtered_count = 0;

$fixme = '';
$array_string = '';

foreach ($sql as $c => $post):

    // messed up post for some reason
    if ($post->ID == 366)
        continue;

    $image_meta = unserialize($post->meta_value);
    if (!$image_meta)
        continue;


    $credit = false;
    if (array_key_exists('image_meta', $image_meta))
        if (array_key_exists($meta_key, $image_meta['image_meta']))
            if ($image_meta['image_meta'][$meta_key] !== '')
                $credit = $image_meta['image_meta'][$meta_key];


    if ($post->post_content === '' && !$credit)
        continue;


    if (!array_key_exists('sizes', $image_meta))
        continue;


    $url = $image_meta['sizes'][$image_size]['file'];
    $date = explode('/', $image_meta['file']);

    $src = "/wp-content/uploads/{$date[0]}/{$date[1]}/$url";

    $caption = $image_meta['image_meta'][$meta_key];

    $caption_html = '';

    if ($caption !== '')
        $caption_html = "<p class='small'>Credit from Image: $caption</p>";

    $title = $post->post_title === '' ? $post->ID : $post->post_title;
    $credit = $post->post_content === '' ? '[none]' : $post->post_content;

    if ($credit === '[none]' && $caption !== ''):
        $fixme .= "<p>{$post->ID} {$caption}</p>";
        $array_string .= "$post->ID, ";
    endif;

    $html .= "
        <div class='cell'>
            <div class='card'>
                <div class='card-section'>
                    <div class='grid-x grid-padding-x align-center-middle'>
                        <div class='cell shrink'>
                            <img src='{$src}' style='margin-bottom: 30px'/>
                        </div>
                        <div class='cell auto'>
                            <p>Image Title : $title</p>
                            <p>Post ID : $post->ID</p>
                            <hr>
                            <p>On Site Credit : $credit</p>
                            $caption_html
                        </div>
                    </div>
                </div>
            </div>
        </div>
    ";

    $filtered_count++;


endforeach;

$op = '';

$op .= "<h3 class='text-center'>Displaying $filtered_count images</h3>";

if (is_user_logged_in())
    $op .= "<h6 class='text-center'>Total Media Files : $total_results</h6>";

$op .= $fixme;
$op .= $array_string;

$op .= "<div class='grid-x grid-padding-x grid-padding-y align-middle small-up-2'>$html</div>";

print $op;