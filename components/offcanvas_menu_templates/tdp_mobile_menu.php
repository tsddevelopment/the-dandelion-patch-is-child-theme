<?
$off_canvas_menu = new TSD_Infinisite\Off_Canvas_Menu();

// TODO: how do we integrate this with the theme gui?
$menus = [];

$menus_to_display = apply_filters("is_off_canvas_menu", $menus);

$theme_menu = get_field("offcanvas_header_menu", "option");

if ($theme_menu)
    $menus_to_display[] = $theme_menu;

ob_start();

do_action("before_is_offcanvas_menu");

foreach ($menus_to_display as $site_menu)
    $off_canvas_menu->print_menu($site_menu);

do_action("after_is_offcanvas_menu");

$menu_output = ob_get_clean();

$header_logo = get_option("options_header_logo",1);

$img = \TSD_Infinisite\Acme::get_image_info_by_id($header_logo);

?>

<div class="full-height grid-x align-middle padding-box tdp-mobile-menu-container" is-editor-style>
    <div class="cell full-height">

        <div class="full-height grid-y">
            <div class="cell shrink">
                <div class="grid-x align-middle">

                    <? if ($img): ?>
                        <div class="cell shrink logo">
                            <a href="<?= site_url() ?>">
                                <img src="<?= $img['sizes']['medium'] ?>" alt="Header Logo" />
                            </a>
                        </div>
                    <? endif ?>

                    <div class="cell auto"></div>
                    <div class="cell shrink">
                        <i class="fa fa-bars primary-text fa-2x" data-close></i>
                    </div>
                </div>
            </div>
            <div class="cell auto flex-column align-center tdp-mobile-menu">
                <?= $menu_output ?>
            </div>
        </div>

    </div>
</div>