<div class="tdp-portfolio-meta">
    <div class="grid-x">
        <div class="cell auto title">
            <p>
                <a href="<?= $post->permalink ?>">
                    <?= $post->post_title ?>
                </a>
            </p>
        </div>
        <div class="cell shrink test">
            <a
                    href="#"
                    style="color: #bd081c"
                    onclick='window.open ("<?= $post->pin_link() ?>",
                            "mywindow",
                            "width=730,height=750"
                            );
                            return false;'

            >
                <i class="fab fa-pinterest"></i>
            </a>
        </div>
    </div>
</div>
