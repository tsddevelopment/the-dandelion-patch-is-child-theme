<?php

$posts = $this->search_query->posts;


$default_template = get_field("is_default_search_result_excerpt_template", "options");
$posts_per_row = get_field("is_post_type_search_post_per_row", "options");

$post_per_row_class = '';

foreach ($posts_per_row as $size => $count):
    if ($count === '')
        continue;
    $post_per_row_class .= "{$size}-up-{$count} ";
endforeach;

$search_term = isset($_GET['swpquery']) ? $_GET['swpquery'] : $_GET['s'];
$found_posts = $this->search_query->found_posts;
$result_word = $found_posts == 1 ? 'result' : 'results';

$module_args = ['module_template' => THEME_URI . 'module_templates/hero/interior/tdp_default.php',
                'module'          => ['acf_fc_layout' => 'hero',
                                      'title'         => "Search the Patch!",]];

$module_args['module']['content_blocks'][0]['wysiwyg'] = "$found_posts Results";

$hero = new \TSD_Infinisite\ACF_Module($module_args);

?>

<span data-editor-style>

    <?= $hero->get_html() ?>
    <p class="text-center">Your search for &ldquo;<?= $search_term ?>&rdquo; returned <?= $found_posts ?> <?= $result_word ?></p>
    <div style="max-width: 300px; margin: auto;">
        <?= get_search_form() ?>
    </div>
    <div class="spacer"></div>

    <div class="grid-container">

        <div class="grid-x grid-padding-x grid-padding-y <?= $post_per_row_class ?>">
            <? foreach ($posts as $post): ?>
                <?
                $custom_template = get_field("is_{$post->post_type}_search_result_excerpt_template", "option");
                $template = $custom_template == '' ? $default_template : $custom_template;

                if (get_class($post) === 'WP_Post')
                    $post = new \TSD_Infinisite\IS_Post($post);

                $post_archive_output = \TSD_Infinisite\IS_Post_Archive::build_twig_template($post, $template);
                ?>
                <div class="cell">
                    <?= $post_archive_output ?>
                </div>

            <? endforeach ?>
        </div>

        <div class="grid-x grid-padding-x grid-padding-y">
            <div class="cell small-12 text-center">
                <?= \TSD_Infinisite\Acme::get_pagination($this->search_query) ?>
                <div class="spacer large"></div>
            </div>
        </div>

    </div>

</span>