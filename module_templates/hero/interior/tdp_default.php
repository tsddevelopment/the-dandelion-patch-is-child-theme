<?


/**
 * a little logic to see if we should show the header elements in the hero
 *
 * $this->module - the acf field group itself
 * $layout - the entire post's layout object
 * $index - the current module's position in the page builder
 *
 */

$_module_title = $this->get("title");
$title = $_module_title ? $_module_title : get_the_title();
$palette = new \TSD_Infinisite\Palette();
$header_align = $this->get('wysiwyg') ? '' : 'text-center';
?>

<div class="tdp_default_hero">

    <div class="grid-x">
        <div class="cell">
            <h1 class="no-margin center-text <?= $header_align ?>"
            ><?= $title ?></h1>
        </div>
    </div>


    <div class="grid-x">

        <? if ($this->get('menus')): ?>
            <div class="cell small-12 hero-menu">

                <? foreach ($this->get('menus') as $menu): ?>
                    <?

                    if ($menu['walker'])
                        $walker_name = "TSD_Infinisite\\" . $menu['walker'];

                    $args = ['menu'            => $menu['name'],
                             'menu_class'      => 'menu dropdown',
                             'items_wrap'      => '<ul id="%1$s" class="%2$s" data-dropdown-menu>%3$s</ul>',
                             'container_class' => 'tdp-submenu'];

                    if ($walker_name)
                        $args['walker'] = new $walker_name();

                    wp_nav_menu($args);


                    ?>

                <? endforeach ?>


            </div>
        <? endif; ?>


    </div>


</div>
