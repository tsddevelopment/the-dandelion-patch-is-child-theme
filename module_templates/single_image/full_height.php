<?
$img = $this->get("image");
if (!$img)
    return;

$width = $this->get('full_width') ? '100%' : 'auto';
$img_src = isset($img['sizes']) ? $img['sizes']['large'] : $img['url'];
$link_html = $this->get("link") ? TSD_Infinisite\Acme::get_link_html_from_acf_module($this->get('link')) : get_fancybox_links($img);


if (substr($img_src, -3, 3) == 'svg'):

    $svg = file_get_contents($img_src);

    print $svg;

    return;
endif;

?>
<div class="is-single_image tdp-full-height-single-image full-height">
    <?= $link_html[0] ?>
    <div class="full-height"
         style="background-image: url(<?= $img_src ?>); background-size: cover">
    </div>
    <?= $link_html[1] ?>
</div>