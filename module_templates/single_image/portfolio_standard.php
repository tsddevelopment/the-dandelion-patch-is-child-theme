<?
$img = $this->get("image");
if (!$img)
    return;

$width = $this->get('full_width') ? '100%' : 'auto';
$size = $this->get("thumbnail_size");
$img_src = isset($size) && $size != '' ? $img['sizes'][$size] : $img['sizes']['medium'];


$link_html = $this->get("link") ? TSD_Infinisite\Acme::get_link_html_from_acf_module($this->get('link')) : get_fancybox_links($img);

?>
<div class="is-single_image tdp-portfolio-img">
    <div class="grid-x">
        <div class="small-12 cell">
            <?= $link_html[0] ?>
            <img
                    class="full-width img"
                    src="<?= $img_src ?>"
            >
            <?= $link_html[1] ?>
        </div>
    </div>
</div>