<? $id = uniqid("tdp_portfolio_"); ?>
<div class="is-multiple_images tdp_portfolio_gallery">
    <div class="show-for-medium gallery-container">
        <? $cols = count($module->get("images")) >= 3 ? 3 : count($module->get("images")) ?>
        <div class="grid-x grid-padding-x grid-padding-y medium-up-<?= $cols ?>">
            <? foreach ($module->get('images') as $c => $image): ?>

                <?
                // TODO: unit test function doesn't put the image sizes on the image, so we need to put conditional fixes in place
                $links = get_fancybox_links($image, 'tdp_portfolio_gallery');
                $url = $image['sizes']['Large Square'];
                ?>
                <div class="cell item-container">
                    <?= $links[0] ?>
                    <img src="<?= $url ?>" alt="" class="full-width" />
                    <?= $links[1] ?>
                </div>
            <? endforeach ?>
        </div>
    </div>
</div>
