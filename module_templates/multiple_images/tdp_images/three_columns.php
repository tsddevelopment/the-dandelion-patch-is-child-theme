<div class="is-multiple_images tdp_multiple_images three_columns">
    <div class="grid-x align-center align-middle grid-padding-x">
        <? foreach ($module->get('images') as $image): ?>
            <?
            $fields = new \TSD_Infinisite\ACF_Helper(get_post($image['ID']));
            $link = $fields::get_link_html_from_acf_module($fields->get("link"));
            // TODO: unit test function doesn't put the image sizes on the image, so we need to put conditional fixes in place
            $url = isset($image['sizes']) ? $image['sizes']['large'] : $image['url'];
            ?>
            <div class="cell small-4 columns">
                <?= $link[0] ?>
                <img src="<?= $url ?>" alt=""/>
                <?= $link[1] ?>
            </div>
        <? endforeach ?>
    </div>
</div>
