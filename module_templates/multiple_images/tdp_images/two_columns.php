<div class="is-multiple_images tdp_multiple_images">
    <div class="grid-x align-center align-middle grid-padding-x">
        <? foreach ($module->get('images') as $image): ?>
            <?
            $fields = new \TSD_Infinisite\ACF_Helper(get_post($image['ID']));
            // TODO: unit test function doesn't put the image sizes on the image, so we need to put conditional fixes in place
            $url = isset($image['sizes']) ? $image['sizes']['Large Square'] : $image['url'];
            $link_html = $this->get("link") ? TSD_Infinisite\Acme::get_link_html_from_acf_module($this->get('link')) : get_fancybox_links($image);

            ?>
            <div class="cell small-6 columns">
                <?= $link_html[0] ?>
                <img src="<?= $url ?>" alt=""/>
                <?= $link_html[1] ?>
            </div>
        <? endforeach ?>
    </div>
</div>
