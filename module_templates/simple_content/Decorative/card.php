<div class="cell full-height tdp-card-module <?= $module->get('class_names') ?>">
    <div class="card full-height">
        <div class="card-section">
            <?= $module->get('simple_content') ?>
        </div>
    </div>
</div>