<div class="light_green_flower_box full-height">
    <div class="is-simple_content full-height <?= $module->get('class_names') ?>">
        <div class="grid-x full-height">
            <div class="small-12 cell flex-column align-center">
                <?= $module->get('simple_content') ?>
            </div>
        </div>
    </div>
</div>
