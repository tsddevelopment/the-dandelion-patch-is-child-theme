<div class="light-green-with-flowers full-height <?= $module->get('class_names') ?>">
    <div class="is-simple_content full-height flex-column align-center">
        <?= $module->get('simple_content') ?>
    </div>
</div>